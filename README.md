## Petunjuk Pengerjaan
1. Setelah berhasil clone masuk ke folder direktori project:   
 ```$ cd blade-challenge```

2. Berikan perintah berikut:   
```$ composer install ```

3. Pada folder bisa ditemukan sebuah file .env-example, copy isi dari file tersebut lalu buat file dengan nama ".env" dan paste ke file ".env" tersebut.

4. setelah selesai perintah sebelumnya, berikan perintah berikut:  
``` $ php artisan key:generate ```

5. Buat branch dengan nama sendiri. 

6. Jalankan server dengan artisan serve:  
``` $ php artisan serve ```
